package ucanuup.cc.config.jpa;

import java.util.HashMap;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import ucanuup.cc.config.AtomikosJtaPlatform;

@Configuration
@DependsOn("transactionManager")
@EnableJpaRepositories(
        entityManagerFactoryRef = "primaryManagerFactory",
        transactionManagerRef = "transactionManager",
        basePackages = {"ucanuup.cc.api.user.dao"})
public class PrimaryConfig {
	
 
    @Resource
    @Qualifier("primaryDataSource")
    private DataSource primaryDataSource;
 
    @Autowired
	private JpaVendorAdapter jpaVendorAdapter;
    
	@Bean(name = "primaryManagerFactory")
	@DependsOn({"transactionManager","primaryDataSource"})
	public LocalContainerEntityManagerFactoryBean customerEntityManager() throws Throwable {

		HashMap<String, Object> properties = new HashMap<String, Object>();
		properties.put("hibernate.transaction.jta.platform", AtomikosJtaPlatform.class.getName());
		properties.put("javax.persistence.transactionType", "JTA");

		LocalContainerEntityManagerFactoryBean entityManager = new LocalContainerEntityManagerFactoryBean();
		entityManager.setJtaDataSource(primaryDataSource);
		entityManager.setJpaVendorAdapter(jpaVendorAdapter);
		entityManager.setPackagesToScan("ucanuup.cc.api.user.entity");
		entityManager.setPersistenceUnitName("customerPersistenceUnit");
		entityManager.setJpaPropertyMap(properties);
		return entityManager;
	}
}