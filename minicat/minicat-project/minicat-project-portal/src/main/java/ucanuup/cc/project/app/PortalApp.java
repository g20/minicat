package ucanuup.cc.project.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ucanuup.cc")
@EnableAutoConfiguration
public class PortalApp {
	public static void main(String[]args) {
		SpringApplication.run(PortalApp.class, args); 
	}
}
