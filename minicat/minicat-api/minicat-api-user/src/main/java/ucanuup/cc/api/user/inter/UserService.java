package ucanuup.cc.api.user.inter;

import org.springframework.stereotype.Service;

import ucanuup.cc.api.user.entity.User;

@Service
public interface UserService {

	User findOne(String id);
	
	User findLogin(User user);
	
	User save(User entity);
}
