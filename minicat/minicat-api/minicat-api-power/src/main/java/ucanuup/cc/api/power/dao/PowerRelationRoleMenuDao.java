package ucanuup.cc.api.power.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import ucanuup.cc.api.power.entity.PowerRelationRoleMenu;

public interface PowerRelationRoleMenuDao   extends CrudRepository<PowerRelationRoleMenu, String>,JpaSpecificationExecutor<PowerRelationRoleMenu> {

}
