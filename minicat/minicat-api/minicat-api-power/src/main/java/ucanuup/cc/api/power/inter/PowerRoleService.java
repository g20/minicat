package ucanuup.cc.api.power.inter;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import ucanuup.cc.api.power.entity.PowerRole;
import ucanuup.cc.common.web.query.BaseQuery;

@Service
public interface PowerRoleService {
	
	PowerRole  save(PowerRole entity);
	
	Iterable<PowerRole> saveAll(Iterable<PowerRole> entities);
	
	Page<PowerRole> queryBaseRole(BaseQuery query) throws Exception ;
}
