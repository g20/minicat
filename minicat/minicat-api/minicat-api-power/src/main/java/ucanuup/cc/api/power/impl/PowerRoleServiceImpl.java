package ucanuup.cc.api.power.impl;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import ucanuup.cc.api.power.entity.PowerRole;
import ucanuup.cc.api.power.inter.PowerRoleService;
import ucanuup.cc.common.web.query.BaseQuery;

/**
 * 管理员权限表管理类
 */
@Service
public class PowerRoleServiceImpl implements PowerRoleService {

	@Override
	public PowerRole save(PowerRole entity) {
		return null;
	}

	@Override
	public Iterable<PowerRole> saveAll(Iterable<PowerRole> entities) {
		return null;
	}

	@Override
	public Page<PowerRole> queryBaseRole(BaseQuery query) throws Exception {
		return null;
	}

}
