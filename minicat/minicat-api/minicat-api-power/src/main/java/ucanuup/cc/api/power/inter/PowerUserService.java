package ucanuup.cc.api.power.inter;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import ucanuup.cc.api.power.entity.PowerUser;
import ucanuup.cc.common.web.query.BaseQuery;

@Service
public interface PowerUserService {

	PowerUser  save(PowerUser entity);
	
	Iterable<PowerUser> saveAll(Iterable<PowerUser> entities);

	Page<PowerUser> queryBaseUser(BaseQuery query) throws Exception ;
	
	Page<PowerUser> queryUserByRole(BaseQuery query) throws Exception ;
}
