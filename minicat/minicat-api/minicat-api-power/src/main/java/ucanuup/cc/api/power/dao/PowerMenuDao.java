package ucanuup.cc.api.power.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import ucanuup.cc.api.power.entity.PowerMenu;

public interface PowerMenuDao extends CrudRepository<PowerMenu, String>,JpaSpecificationExecutor<PowerMenu> {

}
