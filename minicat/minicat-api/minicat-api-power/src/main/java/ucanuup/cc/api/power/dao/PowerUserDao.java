package ucanuup.cc.api.power.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ucanuup.cc.api.power.entity.PowerUser;

public interface PowerUserDao  extends CrudRepository<PowerUser, String>,JpaSpecificationExecutor<PowerUser> {

}
