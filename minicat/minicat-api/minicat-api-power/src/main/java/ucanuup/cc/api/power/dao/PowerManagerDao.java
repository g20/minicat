package ucanuup.cc.api.power.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import ucanuup.cc.api.power.entity.PowerManager;

public interface PowerManagerDao extends CrudRepository<PowerManager, String>,JpaSpecificationExecutor<PowerManager>{

}
