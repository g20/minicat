package ucanuup.cc.api.power.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import ucanuup.cc.api.power.entity.PowerResource;

public interface PowerResourceDao   extends CrudRepository<PowerResource, String>,JpaSpecificationExecutor<PowerResource> {

}
