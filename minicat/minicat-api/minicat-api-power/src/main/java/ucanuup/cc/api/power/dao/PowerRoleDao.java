package ucanuup.cc.api.power.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import ucanuup.cc.api.power.entity.PowerRole;

public interface PowerRoleDao extends CrudRepository<PowerRole, String>,JpaSpecificationExecutor<PowerRole> {

}
