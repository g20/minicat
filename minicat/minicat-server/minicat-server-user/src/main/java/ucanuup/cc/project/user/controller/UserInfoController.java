package ucanuup.cc.project.user.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import ucanuup.cc.common.web.returninfo.RtMsg;
import ucanuup.cc.common.web.returninfo.RtType;
import ucanuup.cc.project.user.model.RegisterModel;

@RestController
@RequestMapping("user/userInfo")
@Api(value = "UserLoginController", description = "用户信息接口，用于个人信息完善（登录人的信息认证填写）", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserInfoController {

	@PostMapping("headpicture")
	@ApiOperation(value = "修改头像信息", notes = "修改头像信息")
	public RtMsg<String> headpicture(RegisterModel model){
		
		return new RtMsg<String>(RtType.VALID,"sdfjslkdfj");
	}
	
	@PostMapping("baseInfo")
	@ApiOperation(value = "修改基本信息", notes = "修改基本信息-节点1")
	public RtMsg<String> baseInfo(@RequestBody @ApiParam(name="修改基本信息对象",value="传入json格式",required=true) RegisterModel model){
		
		return new RtMsg<String>(RtType.VALID,"sdfjslkdfj");
	}
	
	@PostMapping("financeInfo")
	@ApiOperation(value = "修改财务信息", notes = "修改财务信息-节点2")
	public RtMsg<String> financeInfo(@RequestBody @ApiParam(name="修改基本信息对象",value="传入json格式",required=true) RegisterModel model){
		
		return new RtMsg<String>(RtType.VALID,"sdfjslkdfj");
	}
}
