package ucanuup.cc.project.jwt.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import ucanuup.cc.common.web.returninfo.RtMsg;
import ucanuup.cc.common.web.returninfo.RtType;
import ucanuup.cc.project.user.model.LoginInfoModel;

@RestController
@RequestMapping("user/jwt")
@Api(value = "JwtController", description = "jwt-Token", produces = MediaType.APPLICATION_JSON_VALUE)
public class JwtController {

	@PostMapping("login")
	@ApiOperation(value = "jwt-Token登录", notes = "用于jwt登录，认证信息将放在header中")
	public RtMsg<String> update(@RequestBody @ApiParam(name="登录信息格式",value="传入json格式",required=true) LoginInfoModel model){
		
		return new RtMsg<String>(RtType.VALID,"sdfjslkdfj");
	}
}
