package ucanuup.cc.project.user.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import ucanuup.cc.common.web.returninfo.RtMsg;
import ucanuup.cc.common.web.returninfo.RtType;
import ucanuup.cc.project.user.model.RegisterModel;

@RestController
@RequestMapping("user/passwordUpdate")
@Api(value = "PasswordUpdateController", description = "用户密码更新接口", produces = MediaType.APPLICATION_JSON_VALUE)
public class PasswordUpdateController {

	@PostMapping("application")
	@ApiOperation(value = "请求修改，推送改密信息", notes = "用于修改验证")
	public RtMsg<String> application(@RequestBody @ApiParam(name="请求修改密码对象",value="传入json格式",required=true) RegisterModel model){
		
		return new RtMsg<String>(RtType.VALID,"sdfjslkdfj");
	}
	
	@PostMapping("update")
	@ApiOperation(value = "修改密码", notes = "用于修改用户的账户及密码")
	public RtMsg<String> update(@RequestBody @ApiParam(name="修改密码对象",value="传入json格式",required=true) RegisterModel model){
		
		return new RtMsg<String>(RtType.VALID,"sdfjslkdfj");
	}
}
