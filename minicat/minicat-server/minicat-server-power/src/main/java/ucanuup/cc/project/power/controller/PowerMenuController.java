package ucanuup.cc.project.power.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import ucanuup.cc.common.web.returninfo.RtMsg;
import ucanuup.cc.common.web.returninfo.RtType;
import ucanuup.cc.project.power.model.SearchUserModel;

@RestController
@RequestMapping("power/powerMenu")
@Api(value = "PowerMenuController", description = "用户权限菜单服务类", produces = MediaType.APPLICATION_JSON_VALUE)
public class PowerMenuController {

	@PostMapping("queryMenusByUser")
	@ApiOperation(value = "通过用户查询持有菜单信息", notes = "通过用户查询持有菜单信息")
	public RtMsg<String> queryMenusByUser(@RequestBody @ApiParam(name="登出登录",value="传入json格式",required=true) SearchUserModel model){
		return new RtMsg<String>(RtType.VALID,"sdfjslkdfj");
	}
	
	@PostMapping("queryMenusByRole")
	@ApiOperation(value = "通过角色查询持有菜单信息", notes = "通过角色查询持有菜单信息")
	public RtMsg<String> queryMenusByRole(@RequestBody @ApiParam(name="登出登录",value="传入json格式",required=true) SearchUserModel model){
		return new RtMsg<String>(RtType.VALID,"sdfjslkdfj");
	}
	
	@PostMapping("add")
	@ApiOperation(value = "添加菜单", notes = "添加菜单")
	public RtMsg<String> add(@RequestBody @ApiParam(name="对象信息",value="传入json格式",required=true) SearchUserModel model){
		return new RtMsg<String>(RtType.VALID,"sdfjslkdfj");
	}
	
	@PostMapping("update")
	@ApiOperation(value = "更新菜单", notes = "更新菜单")
	public RtMsg<String> update(@RequestBody @ApiParam(name="对象信息 以及传入主键ID",value="传入json格式",required=true) SearchUserModel model){
		return new RtMsg<String>(RtType.VALID,"sdfjslkdfj");
	}
	
	@PostMapping("delete")
	@ApiOperation(value = "删除菜单", notes = "删除菜单")
	public RtMsg<String> delete(@RequestBody @ApiParam(name="资源id， 同时移除相关关系",value="传入json格式",required=true) SearchUserModel model){
		return new RtMsg<String>(RtType.VALID,"sdfjslkdfj");
	}
	
	@PostMapping("addRelation")
	@ApiOperation(value = "添加菜单关联权限", notes = "添加菜单关联权限")
	public RtMsg<String> addRelation(@RequestBody @ApiParam(name="资源关联对象-资源ID 以及 菜单ID",value="传入json格式",required=true) SearchUserModel model){
		return new RtMsg<String>(RtType.VALID,"sdfjslkdfj");
	}
	
	@PostMapping("removeRelation")
	@ApiOperation(value = "移除菜单关联权限", notes = "移除菜单关联权限")
	public RtMsg<String> removeRelation(@RequestBody @ApiParam(name="资源关联对象-资源ID 以及 菜单ID",value="传入json格式",required=true) SearchUserModel model){
		return new RtMsg<String>(RtType.VALID,"sdfjslkdfj");
	}
}
