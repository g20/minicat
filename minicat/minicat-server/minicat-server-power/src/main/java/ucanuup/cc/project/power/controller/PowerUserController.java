package ucanuup.cc.project.power.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import ucanuup.cc.api.power.inter.PowerUserService;
import ucanuup.cc.common.web.returninfo.RtMsg;
import ucanuup.cc.common.web.returninfo.RtType;
import ucanuup.cc.project.power.model.SearchUserModel;

@RestController
@RequestMapping("power/powerUser")
@Api(value = "PowerUserController", description = "用户权限服务类", produces = MediaType.APPLICATION_JSON_VALUE)
public class PowerUserController {

	// 
	@Autowired
	private  PowerUserService  powerUserService;
	
	@PostMapping("queryPowerUser")
	@ApiOperation(value = "查询用户信息", notes = "用于查询用得数据信息")
	public RtMsg<String> queryPowerUser(@RequestBody @ApiParam(name="登出登录",value="传入json格式",required=true) SearchUserModel model){
		return new RtMsg<String>(RtType.VALID,"sdfjslkdfj");
	}
	
	@PostMapping("queryUserByRole")
	@ApiOperation(value = "通过角色查询角色持有用户", notes = "通过角色查询角色持有用户")
	public RtMsg<String> queryUserByRole(@RequestBody @ApiParam(name="登出登录",value="传入json格式",required=true) SearchUserModel model){
		return new RtMsg<String>(RtType.VALID,"sdfjslkdfj");
	}
}
