package ucanuup.cc.project.power.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import ucanuup.cc.common.web.returninfo.RtMsg;
import ucanuup.cc.common.web.returninfo.RtType;
import ucanuup.cc.project.power.model.SearchUserModel;

@RestController
@RequestMapping("power/powerRole")
@Api(value = "PowerRoleController", description = "用户权限角色服务类", produces = MediaType.APPLICATION_JSON_VALUE)
public class PowerRoleController {
	
	@PostMapping("queryRoleByUser")
	@ApiOperation(value = "通过用户查询持有的角色", notes = "通过用户查询持有的角色")
	public RtMsg<String> queryUserByRole(@RequestBody @ApiParam(name="登出登录",value="传入json格式",required=true) SearchUserModel model){
		return new RtMsg<String>(RtType.VALID,"sdfjslkdfj");
	}
	
	@PostMapping("add")
	@ApiOperation(value = "添加角色", notes = "添加角色")
	public RtMsg<String> add(@RequestBody @ApiParam(name="对象信息",value="传入json格式",required=true) SearchUserModel model){
		return new RtMsg<String>(RtType.VALID,"sdfjslkdfj");
	}
	
	@PostMapping("update")
	@ApiOperation(value = "更新角色", notes = "更新角色")
	public RtMsg<String> update(@RequestBody @ApiParam(name="对象信息 以及传入主键ID",value="传入json格式",required=true) SearchUserModel model){
		return new RtMsg<String>(RtType.VALID,"sdfjslkdfj");
	}
	
	@PostMapping("delete")
	@ApiOperation(value = "删除角色", notes = "删除角色")
	public RtMsg<String> delete(@RequestBody @ApiParam(name="资源id， 同时移除相关关系",value="传入json格式",required=true) SearchUserModel model){
		return new RtMsg<String>(RtType.VALID,"sdfjslkdfj");
	}
	
	@PostMapping("clone")
	@ApiOperation(value = "拷贝角色权限", notes = "拷贝角色给另外一个角色")
	public RtMsg<String> clone(@RequestBody @ApiParam(name="资源id， 同时移除相关关系",value="传入json格式",required=true) SearchUserModel model){
		return new RtMsg<String>(RtType.VALID,"sdfjslkdfj");
	}
	
	@PostMapping("cloneUserRoles")
	@ApiOperation(value = "拷贝用户角色权限", notes = "拷贝用户角色给另外一个角色")
	public RtMsg<String> cloneUserRoles(@RequestBody @ApiParam(name="资源id， 同时移除相关关系",value="传入json格式",required=true) SearchUserModel model){
		return new RtMsg<String>(RtType.VALID,"sdfjslkdfj");
	}
}
