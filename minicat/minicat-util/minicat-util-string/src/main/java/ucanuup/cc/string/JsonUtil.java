package ucanuup.cc.string;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtil {
	private static final ObjectMapper mapper = new ObjectMapper();
     
	public static <T>  T  parseObject(String json, Class<T> clazz) throws JsonParseException, JsonMappingException, IOException {
		
		return mapper.readValue(json, clazz);
	}

	public static String toJSONString(Object jsonObj) throws JsonProcessingException {
		
		return mapper.writeValueAsString(jsonObj);
	}

}
