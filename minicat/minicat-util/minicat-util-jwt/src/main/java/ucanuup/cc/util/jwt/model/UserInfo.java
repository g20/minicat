package ucanuup.cc.util.jwt.model;

public class UserInfo {

	private String address;
	
	private String account;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}
}
